

class Picker:
	colors = 'bgrcmykw'
	current = -1

	@staticmethod
	def reset():
		Picker.current = -1
		return Picker

	@staticmethod
	def get() -> str:
		Picker.current += 1
		return Picker.colors[Picker.current]

