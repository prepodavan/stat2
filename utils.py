import numpy as np


def rsq(y, x, mod):
    residuals = y - mod.predict(x)
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((y - np.mean(y))**2)
    return 1 - (ss_res / ss_tot)


