import warnings
import numpy as np
import matplotlib as pltlib
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


class Model:

	def __init__(self, func):
		self.func = func

	def fit(self, *params, **kw):
		warnings.filterwarnings("ignore")
		self.params_optimal, self.params_covariance = curve_fit(self.func, *params, **kw)

	def predict(self, xs):
		return self.func(xs, *self.params_optimal)

	def plot_xy2(self, xs, ys, ind=None, xlabel='X', ylabel='Y'):
		raw_xs = xs
		if ind is not None:
			raw_xs = xs[ind]
		f = plt.figure(figsize=(8, 6), dpi=100)
		axes = f.add_subplot(111)
		axes.plot(raw_xs, ys, 'D')
		axes.set_xlabel(xlabel)
		axes.set_ylabel(ylabel)
		xm = np.linspace(np.min(raw_xs), np.max(raw_xs), num=len(ys))
		ym = self.predict(xs)
		axes.plot(xm, ym)
		plt.show()
		plt.close('all')

